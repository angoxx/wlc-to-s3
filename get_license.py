from winreg import (
    HKEY_LOCAL_MACHINE, CloseKey, OpenKey, QueryValueEx
)
import socket
import boto3
import os
import configparser


def DecodeProductKey(digitalProductId):
    _map = list('BCDFGHJKMPQRTVWXY2346789')
    _key = list(range(0, 29))
    _raw = list(digitalProductId)[52:-97]

    i = 28
    while i >= 0:
        if (i + 1) % 6 == 0:
            _key[i] = '-'
        else:
            k = 0
            j = 14
            while j >= 0:
                k = (k * 256) ^ int(_raw[j])
                _raw[j] = k / 24
                k %= 24
                _key[i] = _map[k]
                j -= 1
        i -= 1

    return ''.join(map(str, _key))


def main():
    """Get the windows license key and upload it tyo S3"""

    # Get the license key
    with OpenKey(HKEY_LOCAL_MACHINE, r'SOFTWARE\Microsoft\Windows NT\CurrentVersion') as ok:
        v, t = QueryValueEx(ok, 'DigitalProductId')
        license_key = DecodeProductKey(v)

    # Get the hostname it exist
    if socket.gethostname():
        hostname = socket.gethostname()

        # Set the .txt name with the hostname
        txt_name = hostname + ".txt"

        # Get the local path
        local_path = os.getcwd()

        # Write the license key in the .txt file
        with open(local_path + "\\" + txt_name, "w+") as txt_file:
            txt_file.write(license_key)

        """ -- BOTO -- """

        s3 = boto3.client('s3', aws_access_key_id="AKIAJK7AKNUNWV6DSJSA", aws_secret_access_key="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")

        # Upload the file into a bucket
        s3.upload_file(local_path + "\\" + txt_name, "license-key-windows", txt_name)

        # Remove the .txt file
        os.remove(local_path + "\\" + txt_name)

if __name__ == '__main__':
    main()




